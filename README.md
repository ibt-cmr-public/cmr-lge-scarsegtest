This repository will contain code relating to the manuscript:
Impact of late gadolinium enhancement image resolution on neural network based automatic scar segmentation in cardiovascular magnetic resonance imaging 
submitted to JCMR for consideration for publishing as Technical note. 
Code will be available upon publication of the articel. 
